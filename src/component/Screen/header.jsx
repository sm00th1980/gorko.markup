import React from 'react';

const Header = () => (
	<div className="header-wrap">
		<a className="header-logo ** js-ajax" href="/">
			<div className="widget-logo-wrap">
				<i className="widget-logo typeWhite ** icon-logo" />
			</div>
		</a>
		<div className="header-city ** js-popover-region">
			<i className="header-city-icon ** icon-header-point" />
			<span className="header-city-link" />
		</div>
		<div className="header-menu ** js-header-menu">
			<ul className="header-menu-list">
				<li className="header-menu-item -login ">
					<a className="header-menu-item-link ** js-header-menu-item">
						<i className="header-menu-item-icon ** icon-menu-login" />
						<span className="header-menu-item-text">{ t('Войти') }</span>
					</a>
				</li>
				<li className="header-menu-item -svadba ">
					<a className="header-menu-item-link ** js-header-menu-item" href="http://www.gorko.ru:5141/wedding/">
						<i className="header-menu-item-icon ** icon-menu-svadba" />
						<span className="header-menu-item-text">{ t('Свадьбы') }</span>
					</a>
				</li>
				<li className="header-menu-item -sets ">
					<a className="header-menu-item-link ** js-header-menu-item" href="http://www.gorko.ru:5141/photos/">
						<i className="header-menu-item-icon ** icon-menu-sets" />
						<span className="header-menu-item-text">{ t('Фотопоток') }</span>
					</a>
				</li>
			</ul>
		</div>
		<a href="javascript:void(0);" className="header-search ** icon-header-search" />
	</div>
);

export default Header;