import React from 'react';

const Footer = () => (
	<div className="region-footer">
		<div className="flex-wrap-bg">
			<div className="flex-wrap">
				<div className="footer">
					<div className="widget-iconWithText-group ** js-region-language"></div>

					<ul className="footer-info">
						<li className="footer-info-i">
							<a href="javascript:void(0);" target="_blank">{ t('Платные сервисы') }</a>
						</li>
						<li className="footer-info-i">
							<a href="javascript:void(0);" target="_blank">{ t('Конфиденциальность') }</a>
						</li>
						<li className="footer-info-i">
							<a href="javascript:void(0);" target="_blank">{ t('Помощь') }</a>
						</li>
						<li className="footer-info-i">
							<a href="javascript:void(0);" target="_blank">{ t('Задать вопрос') }</a>
						</li>
						<li className="footer-info-i">
							<a href="javascript:void(0);" target="_blank">{ t('Блог') }</a>
						</li>
						<li className="footer-info-i">
							<a href="https://vk.com/gorko_ru" className="widget-social size20 typeSocialVK ** icon-social-small" target="_blank"></a>
						</li>
						<li className="footer-info-i">
							<a href="https://www.facebook.com/gorko.ru/" className="widget-social size20 typeSocialFB ** icon-social-small" target="_blank"></a>
						</li>
					</ul>

					<ul className="footer-partner">
						<li className="footer-partner-i">
							<a href="http://plusmedia.ru/" className="link-gray" target="_blank">{ t('Сделано в Плюсмедии') }</a>
						</li>
						<li className="footer-partner-i">
							<a href="http://liderpoiska.ru/" className="link-gray" target="_blank">{ t('Продвижение: Лидер Поиска') }</a>
						</li>
						<li className="footer-partner-i">
							<a href="http://www.mywed.ru/" className="link-gray" target="_blank">{ t('MyWed.com') }</a>
						</li>
						<li className="footer-partner-i">
							<a href="http://wedding.net/" className="link-gray" target="_blank">{ t('Wedding.net') }</a>
						</li>
						<li className="footer-partner-i">
							<a href="http://slubowisko.pl/" className="link-gray" target="_blank">{ t('Slubowisko.pl') }</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
);

export default Footer;