import React from 'react';
import Header from './header';
import Footer from './footer';

const Screen = ({ match, find }) => {
	const {default: Content, Modal} = find(match.params.screenId);

	return (
		<div className="wrapper">
			<div className="region-header"><Header /></div>
			<div className="region-content">{Content && <Content />}</div>
			<Footer />
			<div className="region-modal">{Modal && <Modal />}</div>
		</div>
	);
};

export default Screen;
