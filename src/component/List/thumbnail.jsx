import React from 'react';
import PropTypes from 'prop-types';

const style = { flex: 1, position: 'fixed', top: 0, right: 0, left: '50%', bottom: 0, zIndex: 1 };

const Component = ({ src }) => (
  <div className="screen-test img-screen" style={style}>
    <img src={src} alt="" style={{ maxWidth: '100%', maxHeight: '100%' }} />
  </div>
	);

Component.propTypes = {
	src: PropTypes.string.isRequired,
};

export default Component;
