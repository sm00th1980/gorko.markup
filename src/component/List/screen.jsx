/* eslint "jsx-a11y/no-static-element-interactions": "off" */

import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Link } from 'react-router-dom';

const style = {
	display: 'inline-block',
	cursor: 'pointer',
	margin: '20px 0 0 0',
	fontSize: '22px',
	letterSpacing: '0.5px',
	fontWeight: 300,
};

const Item = (item, index, showThumbnail, hideThumbnail) => (
  <li
    key={index}
    className="links-center-item"
    onMouseEnter={_.partial(showThumbnail, item.name)}
    onMouseLeave={hideThumbnail}
  >
	<Link to={item.path} target="_blank" rel="noopener noreferrer">{item.name}</Link>
  </li>
);

const Component = ({ screen, actions }) => {
	const itemWithActions = _.partial(Item, _, _, actions.showThumbnail, actions.hideThumbnail);
	return (
  <div>
    <h2 style={style} onClick={_.partial(actions.titleClick, screen.items)}>
      {screen.title}
    </h2>
    <ul className="links-center">
      { screen.items.map((item, index) => itemWithActions(item, index))}
    </ul>
  </div>
	);
};

Component.propTypes = {
	screen: PropTypes.shape({
		title: PropTypes.string,
		items: PropTypes.array,
	}),
	actions: PropTypes.shape({
		showThumbnail: PropTypes.func,
		hideThumbnail: PropTypes.func,
	}),
};

export default Component;
