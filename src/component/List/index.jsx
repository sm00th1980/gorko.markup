import React from 'react';
import PropTypes from 'prop-types';
import Helper from '../../utils/helper/';
import Screen from './screen';
import Thumbnail from './thumbnail';
import _ from 'lodash';

class Component extends React.Component {
	constructor(props, context) {
		super(props, context);

		this.state = { src: '' };

    	// binding
		this.showThumbnail = this.showThumbnail.bind(this);
		this.hideThumbnail = this.hideThumbnail.bind(this);
	}

	showThumbnail(src) {
		this.setState({ src: `//gorko.plusmedia.ru/screen-imgs/${src}.png` });
	}

	hideThumbnail() {
		this.setState({ src: '' });
	}

	render() {
		const screenActions = {
			showThumbnail: this.showThumbnail,
			hideThumbnail: this.hideThumbnail,
			titleClick: items => _.each(items, item => window.open(item.path)),
		};
		return (
			<div className="flex-wrap-bg">
				<div className="flex-wrap" style={{ justifyContent: 'flex-start' }}>
					<div className="screen-code" style={{ textAlign: 'center' }}>
						{
							Helper.extractScreens(this.props.paths).map(screen => (
								<Screen screen={screen} key={_.uniqueId()} actions={screenActions} />
							))
						}
					</div>
				</div>
				<Thumbnail src={this.state.src} />
			</div>
		);
	}
}

Component.propTypes = {
	paths: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default Component;
