import React from 'react';
import { render } from 'react-dom';
import _ from 'lodash';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// components
import Screen from './component/Screen/';
import List from './component/List/';

// css importing
//import '../build-css/sass/font.css';
//import '../build-css/sass/main.css';

import Helper from './utils/helper/';

const request = require.context('./screen', true, /^\.\/.*\.jsx$/);
const templates = () => Helper.templates(request.keys());

const MOUNT_POINT = 'root';

//global t()
window.t = value => value;

render(
	<Router>
		<div className="js-router-wrapper">
			<Route exact path="/" component={() => <List paths={templates()} />}/>
			<Route path={`/:screenId`} component={(props) => <Screen {...props} find={_.partial(Helper.findComponentByKey, request)} />}/>
		</div>
	</Router>,
  document.getElementById(MOUNT_POINT),
);
