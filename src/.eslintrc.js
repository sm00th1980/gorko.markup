module.exports = {
  extends: 'react-app',
  rules: {
    "no-unused-vars": 0,
	"no-script-url": 0
  },
  globals: {
    t: false
  }
}