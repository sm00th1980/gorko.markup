import Helper from '../';

describe('duplicates', () => {

	it('should check duplicates for empty list correctly', () => {
		expect(Helper.duplicates([])).toEqual([]);
	});

	it('should check duplicates correctly if duplicates exists', () => {
		const paths = ['./business/ABC123.twig', './business/ABC124.twig', './parent/child/ABC124.twig', './parent/child/qwe345.twig'];

		expect(Helper.duplicates(paths)).toEqual(['*ABC124.twig']);
	});

	it('should check duplicates correctly if no duplicates', () => {
		const paths = ['./business/ABC123.twig', './business/ABC124.twig', './parent/child/qwe345.twig'];

		expect(Helper.duplicates(paths)).toEqual([]);
	});

	it('should check duplicates correctly if duplicates exists in same dir', () => {
		const paths = ['./business/01___njju.twig', './business/04___njju.twig'];

		expect(Helper.duplicates(paths)).toEqual(['*njju.twig']);
	});

	it('should check duplicates correctly if duplicates exists in different dirs', () => {
		const paths = ['./01_Catalogue/01___h7gu.twig', './05_Business/04___h7gu.twig'];

		expect(Helper.duplicates(paths)).toEqual(['*h7gu.twig']);
	});

	it('should check duplicates correctly if duplicates exists in different dirs for JSX', () => {
		const paths = ['./01_Catalogue/01___h7gu.jsx', './05_Business/04___h7gu.jsx'];

		expect(Helper.duplicates(paths)).toEqual(['*h7gu.jsx']);
	});

});
