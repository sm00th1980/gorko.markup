import Helper, {NOT_FOUND_JSX} from '../';

describe('Helper - .findComponentByKey()', () => {

	it('should return not-found component by unknown key', () => {
		const request = jasmine.createSpy('request').and.returnValue('NOT_FOUND_COMPONENT');
		request.keys = () => [];

		const gotResult = Helper.findComponentByKey(request, 'unknown key');

		expect(request).toHaveBeenCalledWith(NOT_FOUND_JSX);
		expect(gotResult).toEqual('NOT_FOUND_COMPONENT');
  });

	it('should return component by key correctly', () => {
		const request = jasmine.createSpy('request').and.returnValue('COMPONENT');
		request.keys = () => ['./business/ABC123.jsx', './business/ABC124.jsx', './parent/child/qwe345.jsx'];

		const gotResult = Helper.findComponentByKey(request, 'ABC124');

		expect(request).toHaveBeenCalledWith('./business/ABC124.jsx');
		expect(gotResult).toEqual('COMPONENT');
  });

});
