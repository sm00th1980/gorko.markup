import Helper, {LIST_TEMPLATE, DEFAULT_TEMPLATE, LANG_SWITCH_TEMPLATE, NOT_FOUND_JSX} from '../';

describe('Helper - .templates()', () => {

	it('should return templates', () => {
		expect(Helper.templates()).toEqual([]);
	});

	it('should remove services templates', () => {
		const paths = [
			'./business/ABC123.twig',
			'./business/ABC124.twig',
			LIST_TEMPLATE,
			DEFAULT_TEMPLATE,
			LANG_SWITCH_TEMPLATE,
			NOT_FOUND_JSX,
			'./parent/child/ABC124.twig',
			'./parent/child/qwe345.twig'
		];

		expect(Helper.templates(paths)).toEqual([
			'./business/ABC123.twig',
			'./business/ABC124.twig',
			'./parent/child/ABC124.twig',
			'./parent/child/qwe345.twig'
		]);
	});

});
