import Helper, {DEFAULT_TEMPLATE} from '../';

describe('findTemplateByKey', () => {

	it('should return default templates', () => {
		const paths = ['./business/ABC123.twig'];

		expect(Helper.findTemplateByKey(paths, 'not-exists')).toBe(DEFAULT_TEMPLATE);
	});

	it('should return template by key correctly', () => {
		const paths = ['./business/ABC123.twig', './business/ABC124.twig', './parent/child/qwe345.twig'];

		expect(Helper.findTemplateByKey(paths, 'ABC124')).toBe('./business/ABC124.twig');
	});

	it('should return first matching template if duplicates', () => {
		const paths = ['./business/ABC123.twig', './business/ABC124.twig', './parent/child/ABC124.twig', './parent/child/qwe345.twig'];

		expect(Helper.findTemplateByKey(paths, 'ABC124')).toBe('./business/ABC124.twig');
	});

	it('should return template with prefix by key correctly', () => {
		const paths = ['./business/02___1ula.twig', './business/01___44d2.twig', './parent/child/01___5crb.twig'];

		expect(Helper.findTemplateByKey(paths, '44d2')).toBe('./business/01___44d2.twig');
	});

});