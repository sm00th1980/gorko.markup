import Helper, {START_SPACELESS, END_SPACELESS, SEPARATOR} from '../';

describe('Helper - .splitBySeparator()', () => {

	it('should return content only', () => {
		const text = 'content';

		expect(Helper.splitBySeparator(text)).toEqual({
			content: START_SPACELESS + text + END_SPACELESS
		});
	});

	it('should return content only', () => {
		const text = START_SPACELESS + 'content' + END_SPACELESS;

		expect(Helper.splitBySeparator(text)).toEqual({
			content: text
		});
	});

	it('should return content and modal', () => {
		const text = `content${SEPARATOR}modal`;

		expect(Helper.splitBySeparator(text)).toEqual({
			content: `${START_SPACELESS}content${END_SPACELESS}`,
			modal: `${START_SPACELESS}modal${END_SPACELESS}`
		});
	});

	it('should return modal', () => {
		const text = `${SEPARATOR}modal`;

		expect(Helper.splitBySeparator(text)).toEqual({
			modal: `${START_SPACELESS}modal${END_SPACELESS}`
		});
	});

	it('should return content and modal', () => {
		const text = `${START_SPACELESS}content${END_SPACELESS}${SEPARATOR}modal`;

		expect(Helper.splitBySeparator(text)).toEqual({
			content: `${START_SPACELESS}content${END_SPACELESS}`,
			modal: `${START_SPACELESS}modal${END_SPACELESS}`
		});
	});

	it('should return content and modal', () => {
		const text = `content${SEPARATOR}${START_SPACELESS}modal${END_SPACELESS}`;

		expect(Helper.splitBySeparator(text)).toEqual({
			content: `${START_SPACELESS}content${END_SPACELESS}`,
			modal: `${START_SPACELESS}modal${END_SPACELESS}`
		});
	});

	it('should return content and modal', () => {
		const text = '{% spaceless %}content{% endspaceless %}\n{% block region-modal %}{% endblock %}\nmodal';

		expect(Helper.splitBySeparator(text)).toEqual({
			content: '{% spaceless %}content{% endspaceless %}',
			modal: '{% spaceless %}modal{% endspaceless %}'
		});
	});

	it('should return content and modal', () => {
		const text = '{% spaceless %}content{% endspaceless %}\r\n{% block region-modal %}{% endblock %}\r\nmodal';

		expect(Helper.splitBySeparator(text)).toEqual({
			content: '{% spaceless %}content{% endspaceless %}',
			modal: '{% spaceless %}modal{% endspaceless %}'
		});
	});

});