import Helper from '../';

describe('screen helper', () => {

	it('should extract empty screens correctly', () => {
		const screens = Helper.extractScreens([]);
		expect(screens).toEqual([]);
	});

	it('should extract screens with uniq items correctly', () => {
		const paths = [
			"./02___business/01___ABC123.twig",
			"./02___business/02___ABC124.twig",
			"./03___parent/child/01___qwe345.twig"
		];

		const screens = Helper.extractScreens(paths);
		expect(screens).toEqual([{
			title: 'business',
			items: [{
				title: 'business',
				name: 'ABC123',
				path: '/ABC123/'
			}, {
				title: 'business',
				name: 'ABC124',
				path: '/ABC124/'
			}]
		}, {
			title: 'parent',
			items: [{
				title: 'parent',
				name: 'qwe345',
				path: '/qwe345/'
			}]
		}]);
	});

	it('should extract screens with non uniq items correctly', () => {
		const paths = [
			"./01___business/01___ABC123.twig",
			"./01___business/02___ABC124.twig",
			"./03___parent/child/01___ABC124.twig",
			"./03___parent/child/02___qwe345.twig"
		];

		const screens = Helper.extractScreens(paths);
		expect(screens).toEqual([{
			title: 'business',
			items: [{
				title: 'business',
				name: 'ABC123',
				path: '/ABC123/'
			}, {
				title: 'business',
				name: 'ABC124',
				path: '/ABC124/'
			}]
		}, {
			title: 'parent',
			items: [{
				title: 'parent',
				name: 'ABC124',
				path: '/ABC124/'
			}, {
				title: 'parent',
				name: 'qwe345',
				path: '/qwe345/'
			}]
		}]);
	});

	it('should sort items using prefix in title', () => {
		const paths = [
			"./02___Business — PRO/02___1ula.twig",
			"./02___Business — PRO/01___44d2.twig",
			"./01___settings/accounts/02___n1s3.twig",
			"./01___settings/accounts/01___5crb.twig",
			"./03___service/mhf9.twig"
		];

		const screens = Helper.extractScreens(paths);
		expect(screens).toEqual([{
			title: 'settings',
			items: [{
				title: 'settings',
				name: '5crb',
				path: '/5crb/'
			}, {
				title: 'settings',
				name: 'n1s3',
				path: '/n1s3/'
			}]
		}, {
			title: 'Business — PRO',
			items: [{
				title: 'Business — PRO',
				name: '44d2',
				path: '/44d2/'
			}, {
				title: 'Business — PRO',
				name: '1ula',
				path: '/1ula/'
			}]
		}, {
			title: 'service',
			items: [{
				title: 'service',
				name: 'mhf9',
				path: '/mhf9/'
			}]
		}]);
	});

});