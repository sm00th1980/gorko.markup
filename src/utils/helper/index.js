import _ from 'lodash';
import { compose } from 'lodash/fp';

import startsWith from 'underscore.string/startsWith';
import endsWith from 'underscore.string/endsWith';
import replaceAll from 'underscore.string/replaceAll';

export const LIST_TEMPLATE = './list.twig';
export const DEFAULT_TEMPLATE = './notfound.twig';
export const LANG_SWITCH_TEMPLATE = './langSwitch.twig';

export const SEPARATOR = '{% block region-modal %}{% endblock %}';
export const START_SPACELESS = '{% spaceless %}';
export const END_SPACELESS = '{% endspaceless %}';

export const NOT_FOUND_JSX = './notFound.jsx';

const wrapInSpacess = (text) => {
	if (startsWith(text, START_SPACELESS) && endsWith(text, END_SPACELESS)) {
		return text;
	}

	if (startsWith(text, START_SPACELESS)) {
		return `${text}${END_SPACELESS}`;
	}

	if (endsWith(text, END_SPACELESS)) {
		return `${START_SPACELESS}${text}`;
	}

	return `${START_SPACELESS}${text}${END_SPACELESS}`;
};

const screenUrl = (screenID) => `/${screenID}/`;

export default class {

	static duplicates(paths) {
		const names = paths
				.map(path => _.last(path.split('/')))
				.map(file => _.last(file.split('___')));
		const counts = _.toPairs(_.countBy(names, el => el));

		return _(counts)
				.filter(el => _.last(el) > 1)
				.map(el => `*${_.first(el)}`)
				.value();
	}

	static templates(paths) {
		return _.reject(paths, path => _.includes([
			LIST_TEMPLATE,
			DEFAULT_TEMPLATE,
			LANG_SWITCH_TEMPLATE,
			NOT_FOUND_JSX,
		], path));
	}

	static findTemplateByKey(templates, key) {
		const item = _.find(templates, path => `${key}.twig` === _.last(_.last(path.split('/')).split('___')));

		return item || DEFAULT_TEMPLATE;
	}

	static findComponentByKey(request, key) {
		const screenPath = _.find(request.keys(), path => `${key}.jsx` === _.last(_.last(path.split('/')).split('___')));

		return screenPath && request(screenPath) ? request(screenPath) : request(NOT_FOUND_JSX);
	}

	static splitBySeparator(text) {
		const removeRN = txt => replaceAll(txt, '\r\n', '');
		const removeN = txt => replaceAll(txt, '\n', '');
		const cleaning = compose(removeN, removeRN);

		const [content, modal] = cleaning(text).split(SEPARATOR);

		if (content && modal) {
			return { content: wrapInSpacess(content), modal: wrapInSpacess(modal) };
		}

		if (content) {
			return { content: wrapInSpacess(content) };
		}

		if (modal) {
			return { modal: wrapInSpacess(modal) };
		}

		return {};
	}

	static extractScreens(paths) {
		let _paths = paths.map((elem) => {
			const elements = _.reject(elem.split('/'), el => el === '.');
			const name = _.first(_.last(elements).split('.'));
			const path = screenUrl(_.last(name.split('___')));
			return { title: _.first(elements), name, path };
		});

		// sorting
		_paths = _.sortBy(_paths, (path) => {
			const sortIndex = _.first(path.title.split('___'));
			return parseInt(sortIndex, 10);
		});

		// remove sorting postfix
		_paths = _paths.map(path => ({ title: _.last(path.title.split('___')), name: path.name, path: path.path }));

		// uniq titles
		const titles = _.uniq(_paths.map(path => path.title));

		const screens = titles.map((title) => {
			let _items = _.filter(_paths, path => path.title === title);

			// sorting
			_items = _.sortBy(_items, (item) => {
				const sortIndex = _.first(item.name.split('___'));
				return parseInt(sortIndex, 10);
			});

			// remove sorting postfix
			_items = _items.map(item => ({ title: item.title, name: _.last(item.name.split('___')), path: item.path }));

			return { title, items: _items };
		});

		return screens;
	}

}
