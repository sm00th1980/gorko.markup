import React from 'react';
import Btn   from 'component/Btn/';
import Form  from 'component/Form/';
import Radio from 'component/Radio/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<SubMenu data={SubMenuData} />

		<div className="flex-wrap-bg typeActionPanel">
			<div className="flex-wrap">
				<div className="actionPanel-l"></div>
				<div className="actionPanel-title">{t('PRO-аккаунт')}</div>
				<div className="actionPanel-r"></div>
			</div>
		</div>

		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<div className="app-pro">
					<span className="app-pro-title" dangerouslySetInnerHTML={{__html: t('PRO активирован 9 сентября на 3 месяца. Осталось <b>90 дней</b>')}} />
					<div className="app-pro-status">
						<span className="app-pro-status-bar" style={{width: '96%'}} />
					</div>

					<span className="app-pro-title" dangerouslySetInnerHTML={{__html: t('PRO активирован 9 сентября на 3 месяца. Осталось <b>44 дня</b>')}} />
					<div className="app-pro-status">
						<span className="app-pro-status-bar typeGold" style={{width: '50%'}} />
					</div>

					<span className="app-pro-title" dangerouslySetInnerHTML={{__html: t('PRO активирован 9 сентября на 3 месяца. Осталось <b>5 дней</b>')}} />
					<div className="app-pro-status">
						<span className="app-pro-status-bar typeRed" style={{width: '10%'}} />
					</div>

					<span className="app-pro-title typeRed" dangerouslySetInnerHTML={{__html: t('PRO истёк 31 сентября')}} />
					<div className="app-pro-status">
						<span className="app-pro-status-bar" style={{width: '0%'}} />
					</div>
				</div>
			</div>
		</div>
	</div>
);