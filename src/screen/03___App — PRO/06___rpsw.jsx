import React from 'react';
import Btn   from 'component/Btn/';
import Form  from 'component/Form/';
import Radio from 'component/Radio/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<SubMenu data={SubMenuData} />

		<div className="flex-wrap-bg typeActionPanel">
			<div className="flex-wrap">
				<div className="actionPanel-l"></div>
				<div className="actionPanel-title">{t('PRO-аккаунт')}</div>
				<div className="actionPanel-r"></div>
			</div>
		</div>

		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<div className="app-pro">
					<span className="app-pro-title" dangerouslySetInnerHTML={{__html: t('Активируйте PRO')}} />

					<Form klass="app-pro-card-l" link="appProPeriod">
						<div className="app-pro-card">
							<Radio />
							<span className="app-pro-card-period">{t('3 месяца')}</span>
							<span className="app-pro-card-bill">6000 ₽</span>
						</div>
						<div className="app-pro-card">
							<Radio />
							<span className="app-pro-card-period">{t('6 месяцев')}</span>
							<span className="app-pro-card-bill">20 000 ₽</span>
						</div>
						<div className="app-pro-card">
							<Radio />
							<span className="app-pro-card-period">{t('12 месяцев')}</span>
							<span className="app-pro-card-bill">60 000 ₽</span>
						</div>
					</Form>

					<span className="app-pro-title typeSmall">{t('Основные преимущества PRO')}</span>
					<div className="app-pro-desc">
						{t('Подключение опции Города (возможность поиска заказов в нескольких городах).')}<br />
						{t('Отображение в каталоге выше участников без PRO.')}
					</div>
				</div>
			</div>
		</div>
	</div>
);