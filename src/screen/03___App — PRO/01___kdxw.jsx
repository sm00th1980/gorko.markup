import React from 'react';
import Btn   from 'component/Btn/';
import Form  from 'component/Form/';
import Radio from 'component/Radio/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<SubMenu data={SubMenuData} />

		<div className="flex-wrap-bg typeActionPanel">
			<div className="flex-wrap">
				<div className="actionPanel-l"></div>
				<div className="actionPanel-title">{t('PRO-аккаунт')}</div>
				<div className="actionPanel-r"></div>
			</div>
		</div>

		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<div className="app-pro">
					<span className="app-pro-title" dangerouslySetInnerHTML={{__html: t('Активация PRO доступна владельцам аккаунтов, соответствующим критериям качества Горько!')}} />

					<div className="app-pro-pane">
						<div className="app-pro-pane-l">
							<div className="app-pro-pane-step">
								<i className="ic-status size24" />
								<span className="app-pro-pane-step-desc">{t('Заполните профиль')}</span>
							</div>
							<div className="app-pro-pane-step">
								<i className="ic-status size24 itemNonSelected">2</i>
								<span className="app-pro-pane-step-desc">{t('Пройдите проверку')}</span>
							</div>
						</div>
						<div className="app-pro-pane-r">
							<div className="app-pro-pane-status">
								<i className="ic-status" />
								<span className="app-pro-pane-status-desc">{t('Заполните личные данные')}</span>
							</div>
							<div className="app-pro-pane-status">
								<i className="ic-status itemNonSelected" />
								<span className="app-pro-pane-status-desc">{t('Укажите стоимость услуг')}</span>
							</div>
							<div className="app-pro-pane-status">
								<i className="ic-status itemNonSelected" />
								<span className="app-pro-pane-status-desc">{t('Добавьте портфолио')}</span>
							</div>
						</div>
					</div>

					<span className="app-pro-title typeSmall">{t('Основные преимущества PRO')}</span>
					<div className="app-pro-desc">
						{t('Подключение опции Города (возможность поиска заказов в нескольких городах).')}<br />
						{t('Отображение в каталоге выше участников без PRO.')}
					</div>
				</div>
			</div>
		</div>
	</div>
);