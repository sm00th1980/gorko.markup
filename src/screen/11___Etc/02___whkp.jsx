import React from 'react';
import Btn   from 'component/Btn/';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<div className="widget-logo-wrap ** margin-tb-60">
					<i className="widget-logo ** icon-logo"></i>
				</div>

				<span className="auth-switch-title">{t('Выберите аккаунт')}</span>

				<div className="auth-switch-l">
					<div className="auth-switch" role="button" tabIndex="0">
						<div className="widget-avatar-wrap size50 textSize295">
							<a className="widget-avatar-img-link" href="javascript:void(0);">
								<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
							</a>
							<div className="widget-avatar-info">
								<span className="widget-avatar-name">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</span>
								<div className="widget-avatar-role">Организатор, Санкт-Петербургггггггггггггггггггггггггггггг</div>
							</div>
						</div>
						<i className="ic-arrow size12 itemDirR ** icon-arrow"></i>
					</div>

					<div className="auth-switch" role="button" tabIndex="0">
						<div className="widget-avatar-wrap size50 textSize295">
							<a className="widget-avatar-img-link" href="javascript:void(0);">
								<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
							</a>
							<div className="widget-avatar-info">
								<span className="widget-avatar-name">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</span>
								<div className="widget-avatar-role">Организатор, Санкт-Петербургггггггггггггггггггггггггггггг</div>
							</div>
						</div>
						<i className="ic-arrow size12 itemDirR ** icon-arrow"></i>
					</div>

					<div className="auth-switch" role="button" tabIndex="0">
						<div className="widget-avatar-wrap size50 textSize295">
							<a className="widget-avatar-img-link" href="javascript:void(0);">
								<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
							</a>
							<div className="widget-avatar-info">
								<span className="widget-avatar-name">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</span>
								<div className="widget-avatar-role">Организатор, Санкт-Петербургггггггггггггггггггггггггггггг</div>
							</div>
						</div>
						<i className="ic-arrow size12 itemDirR ** icon-arrow"></i>
					</div>

					<a className="widget-iconWithText-wrap link-blue" href="javascript:void(0);" role="button" tabIndex="0">
						<i className="widget-newIconMP widget-iconWithText size20 ** icon-plus"></i>
						<span className="widget-iconWithText-value">{t('Создать ещё один аккаунт')}</span>
					</a>
				</div>

				<Btn value={t('Выйти')} klass="btn-small btn-sky ** width-140" />
			</div>
		</div>
	</div>
);