import React      from 'react';
import Btn        from 'component/Btn/';
import ModalTitle from 'component/ModalTitle/';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal">
				<ModalTitle value={t('Вход на Горько!')} />

				<span className="auth-desc itemNonActive">
					{t('Войдите, используя свой аккаунт в социальной сети.')}
				</span>

				<ul className="auth-social-l">
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthVK  itemNonActive ** icon-social"></i>
						<span className="auth-social-name">{t('Вконтакте')}</span>
					</li>
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthFB  itemNonActive ** icon-social"></i>
						<span className="auth-social-name">{t('Facebook')}</span>
					</li>
					<li className="auth-social">
						<i className="ic-social size160 typeSocialAuthODN ** icon-social"></i>
					</li>
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthML  itemNonActive ** icon-social"></i>
						<span className="auth-social-name">{t('Mail.ru')}</span>
					</li>
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthGOO itemNonActive ** icon-social"></i>
						<span className="auth-social-name">{t('Google+')}</span>
					</li>
				</ul>

				<div className="auth-social-desc">
					{t('Вход через')}
					<span className="auth-social-member typeSocialAuthODN">{t(' #social_name ')}</span>
					{t('временно недоступен')}
				</div>
			</div>
		</div>
	</div>
);