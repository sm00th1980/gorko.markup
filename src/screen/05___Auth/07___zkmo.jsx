import React      from 'react';
import Btn        from 'component/Btn/';
import ModalTitle from 'component/ModalTitle/';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal">
				<ModalTitle value={t('Вход на Горько!')} />

				<span className="auth-desc">
					{t('Войдите, используя свой аккаунт в социальной сети.')}
				</span>

				<div className="auth-login">
					<span className="auth-login-desc">
						{t('Если вы регистрировались на предыдущей версии Wedpro.com.ua, воспользуйтесь входом с помощью вашего логина и пароля')}
					</span>

					<Btn value={t('Войти с паролем')} klass="btn-base btn-green-f" />
				</div>

				<ul className="auth-social-l">
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthVK  ** icon-social"></i>
						<span className="auth-social-name">{t('Вконтакте')}</span>
					</li>
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthFB  ** icon-social"></i>
						<span className="auth-social-name">{t('Facebook')}</span>
					</li>
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthODN ** icon-social"></i>
						<span className="auth-social-name">{t('Одноклассники')}</span>
					</li>
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthML  ** icon-social"></i>
						<span className="auth-social-name">{t('Mail.ru')}</span>
					</li>
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthGOO ** icon-social"></i>
						<span className="auth-social-name">{t('Google+')}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
);