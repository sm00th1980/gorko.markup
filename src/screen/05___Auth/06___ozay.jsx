import React      from 'react';
import Btn        from 'component/Btn/';
import ModalTitle from 'component/ModalTitle/';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal">
				<ModalTitle value={t('Вход на Горько!')} />

				<span className="auth-desc itemNonActive">
					{t('Войдите, используя Facebook.')}
				</span>

				<ul className="auth-social-l">
					<li className="auth-social">
						<i className="ic-social size120 typeSocialAuthFB  ** icon-social"></i>
						<span className="auth-social-name">{t('Facebook')}</span>
					</li>
				</ul>

				<div className="auth-social-desc">
					{t('Вы еще не входили с помощью аккаунта')}
					<span className="auth-social-member typeSocialAuthFB">{t('Evgeniy Kozachenko')}</span>
				</div>

				<Btn value={t('Создать аккаунт на <i>Горько!</i>')} klass="btn-big btn-gray-f" mode="waiting" />
			</div>
		</div>
	</div>
);