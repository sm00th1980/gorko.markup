import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import BtnGroup   from 'component/BtnGroup/'
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<SubMenu data={SubMenuData} />

		<div className="flex-wrap-bg typeActionPanel">
			<div className="flex-wrap">
				<div className="actionPanel-l"></div>
				<div className="actionPanel-title">{t('Баланс')}</div>
				<div className="actionPanel-r">
					<Btn value={t('Пополнение купоном')} klass="btn-small btn-sky-f" />
				</div>
			</div>
		</div>

		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumn">
				<div className="app-balance">
					<div className="app-balance-total">
						<span className="app-balance-count">0</span>
						<span className="app-balance-curr">₽</span>
					</div>
				</div>

				<span className="app-balance-hint">
					{t('Баланс отражает текущее состояние персонального счета пользователя Горько! Мы используем пользовательский счет для возврата средств за отмененные или заблокированные заказы, также средствами со счета можно оплачивать услуги на сайте.')}
				</span>
			</div>
		</div>
	</div>
);