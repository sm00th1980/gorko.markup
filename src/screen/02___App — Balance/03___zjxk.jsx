import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import BtnGroup   from 'component/BtnGroup/'
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<SubMenu data={SubMenuData} />

		<div className="flex-wrap-bg typeActionPanel">
			<div className="flex-wrap">
				<div className="actionPanel-l"></div>
				<div className="actionPanel-title">{t('Баланс')}</div>
				<div className="actionPanel-r">
					<Btn value={t('Пополнение купоном')} klass="btn-small btn-sky-f" />
				</div>
			</div>
		</div>

		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumn">
				<div className="app-balance">
					<div className="app-balance-total">
						<span className="app-balance-count">62 800</span>
						<span className="app-balance-curr">₽</span>
					</div>

					<div className="app-balance-info">
						{t('50 000 ₽ заблокировано')}
						<i className="widget-iconHelp size15 ** icon-help" />
						<a className="" id="" href="javascript:void(0);">{t('Отменить вывод средств')}</a>
					</div>
				</div>

				<span className="app-balance-title">{t('Список операций')}</span>

				<div className="tb ** -sectBalance">
					<div className="app-pane">
						<div className="tb-row">
							<div className="tb-cell">Активирован город Тольятти с 29 июня 2016 по 30 июня 2017</div>
							<div className="tb-cell itemNonActive">19 Октября, 2016</div>
							<div className="tb-cell typeRed">– 2000 RUB</div>
						</div>

						<div className="tb-row">
							<div className="tb-cell">Пополнение администратором</div>
							<div className="tb-cell itemNonActive">19 Июля, 2016</div>
							<div className="tb-cell">200 RUB</div>
						</div>

						<div className="tb-row">
							<div className="tb-cell">Coupon activated "E20e5d"</div>
							<div className="tb-cell itemNonActive">27 Мая, 2016</div>
							<div className="tb-cell">40 RUB</div>
						</div>

						<div className="tb-row">
							<div className="tb-cell">QIWI Wallet</div>
							<div className="tb-cell itemNonActive">31 Декабря, 2016</div>
							<div className="tb-cell">3400 RUB</div>
						</div>

						<div className="tb-row">
							<div className="tb-cell">Coupon activated "E20e5d"</div>
							<div className="tb-cell itemNonActive">14 Ноября, 2016</div>
							<div className="tb-cell">24 000 RUB</div>
						</div>

						<div className="tb-row">
							<div className="tb-cell">Coupon activated "E20e5d"</div>
							<div className="tb-cell itemNonActive">20 Июня, 2016</div>
							<div className="tb-cell">10 RUB</div>
						</div>
					</div>
				</div>

				<div className="photo-manage">
					<Btn value={t('Показать ещё')} klass="btn-base btn-sky ** width-300" />

					<div className="widget-iconWithText-group">
						<a className="widget-iconWithText-wrap link-blue" href="javascript:void(0);" role="button" tabIndex="0" download>
							<i className="widget-iconDownload widget-iconWithText size18 ** icon-download" />
							<span className="widget-iconWithText-value">{t('Экспорт в MS Excel')}</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
);