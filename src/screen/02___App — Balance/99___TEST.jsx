import React from 'react';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		CONTENT
	</div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		MODAL
	</div>
);