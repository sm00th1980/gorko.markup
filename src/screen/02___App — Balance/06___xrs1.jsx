import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import BtnGroup   from 'component/BtnGroup/'
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal ** width-660">
				<ModalTitle value={t('Вывод средств')} />

				<div className="modal-notice typeSmall">
					{t('Собственные средства с вашего персонального счета могут быть частично или полностью выведены на банковскую карту. Указанная сумма будет автоматически заблокирована на балансе и после одобрения заявки будет осуществлен перевод на карту.')}
				</div>

				<Form klass="width-270">
					<div className="form-line">
						<FormGroup klass="typeColumn" link="transfer">
							<Input klass="typeCenter" value="2000" placeholder="Сумма перевода" />
							<span className="form-hint">{t('Доступно для перевода: 8000 RUB')}</span>
						</FormGroup>
					</div>
				</Form>

				<div className="app-balance-creditCard">
					<span className="app-balance-creditCard-title">{t('Номер вашей карты')}</span>
					<Form>
						<div className="form-line">
							<FormGroup link="creditCard">
								<Input klass="typeCenter" value="5576 2638 9164 6173" placeholder="16 или 18 цифр" />
							</FormGroup>
						</div>
					</Form>
				</div>

				<Btn value={t('Отправить запрос')} klass="btn-base btn-green-f ** width-220" />

				<div className="modal-footer">
					{t('Срок рассмотрения заявки — 5 рабочих дней. Вывод недоступен для средств, полученных с помощью купона. Услуга предоставляется согласно ')}
					<a className="" id="" href="javascript:void(0);" target="_blank">{t('оферте')}</a>
				</div>
			</div>
		</div>
	</div>
);