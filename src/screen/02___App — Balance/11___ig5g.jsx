import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import BtnGroup   from 'component/BtnGroup/'
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal ** width-600">
				<ModalTitle value={t('Пополнение купоном')} />

				<div className="modal-notice">
					{t('Баланс успешно пополнен на 200 ₽')}
				</div>

				<Btn value={t('Закрыть')} klass="btn-base btn-sky ** width-160" />
			</div>
		</div>
	</div>
);