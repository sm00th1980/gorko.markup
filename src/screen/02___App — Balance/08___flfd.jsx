import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import BtnGroup   from 'component/BtnGroup/'
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal ** width-600">
				<ModalTitle value={t('Отмена вывода средств')} />

				<div className="modal-notice">
					{t('Вы действительно хотите отменить вывод средств?')}
				</div>

				<BtnGroup>
					<Btn value={t('Не отменять')}  klass="btn-base btn-sky     ** width-180" />
					<Btn value={t('Да, отменить')} klass="btn-base btn-green-f ** width-180" />
				</BtnGroup>
			</div>
		</div>
	</div>
);