import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import BtnGroup   from 'component/BtnGroup/'
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SubMenu     from 'component/static/SubMenu';
import SubMenuData from './data/SubMenuData';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal ** width-600">
				<ModalTitle value={t('Пополнение купоном')} />

				<div className="modal-notice">
					{t('Если вы активно продвигаете свои свадебные услуги, то наверняка оплачивали размещение на сайтах ')}
					<a className="" id="" href="http://svadba-msk.ru/" target="_blank">svadba-msk.ru</a>{t(' и ')}<a className="" id="" href="https://mywed.ru/" target="_blank">mywed.ru</a>
					{t('. Это значит, что вам доступен купон на пополнение баланса!')}
				</div>

				<Form klass="width-320 margin-b-20">
					<div className="form-line">
						<FormGroup link="coupon">
							<Input klass="typeCenter" value="6688" placeholder="Введите 6-значный код купона" errorR="Неверный формат" />
						</FormGroup>
					</div>
				</Form>

				<Btn value={t('Отправить')} klass="btn-base btn-green-f ** width-160" />
			</div>
		</div>
	</div>
);