import React     from 'react';
import Btn       from 'component/Btn/';
import Form      from 'component/Form/';
import Check     from 'component/Check/';
import Input     from 'component/Input/';
import Select    from 'component/Select/';
import FormGroup from 'component/FormGroup/';

import SubMenu             from 'component/static/SubMenu';
import SubMenuData         from './data/SubMenuData';
import SelectDataAuto      from './data/SelectDataAuto';
import SelectDataPeriod    from './data/SelectDataPeriod';
import SelectDataDayStart  from './data/SelectDataDayStart';
import SelectDataDayFinish from './data/SelectDataDayFinish';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<SubMenu data={SubMenuData} />

		<div className="flex-wrap-bg typeActionPanel">
			<div className="flex-wrap">
				<div className="actionPanel-l"></div>
				<div className="actionPanel-title">{t('Статистика')}</div>
				<div className="actionPanel-r"></div>
			</div>
		</div>

		<div className="flex-wrap-bg typeSeparate">
			<div className="flex-wrap">
				<Form klass="app-manage">
					<div className="form-line typeSmall">
						<FormGroup link="filter">
							<Select klass="** widthFlex-200" value="День" items={SelectDataPeriod} />
							<Select klass="typeCal ** widthFlex-200" items={SelectDataDayStart} disabled />
							<Check value={t('Посмотрели контакты')} checked />
						</FormGroup>

						<Btn value={t('Показать')} klass="btn-small btn-green-f" />
					</div>
				</Form>
			</div>
		</div>

		<div className="flex-wrap-bg typeOverflow">
			<div className="flex-wrap">
				<div className="tb ** -sectStat">
					<div className="tb-row tb-title">
						<div className="tb-cell typeLong">{t('Посетители')}</div>
						<div className="tb-cell">{t('Время')}</div>
						<div className="tb-cell">{t('Дата свадьбы')}</div>
						<div className="tb-cell typeShort">{t('Просмотры контактов')}</div>
						<div className="tb-cell typeShort">{t('Просмотры фотографий')}</div>
						<div className="tb-cell typeShort" dangerouslySetInnerHTML={{__html: t('Просмотры<br /> в разделе фото')}}></div>
					</div>

					<div className="app-pane">
						<div className="tb-row">
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 textSize170 typeBlank">
									<span className="widget-avatar-img" />
									<div className="widget-avatar-info">
										<span className="widget-avatar-name">{t('Неавторизованный')}</span>
									</div>
								</div>
							</div>
							<div className="tb-cell">Сегодня, 2:23</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
							<div className="tb-cell typeShort"><i className="ic-status itemNonActive" /></div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 typeBlack textSize170">
									<a className="widget-avatar-img-link" href="javascript:void(0);">
										<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
									</a>
									<div className="widget-avatar-info">
										<a className="widget-avatar-name" href="javascript:void(0);">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</a>
									</div>
								</div>
							</div>
							<div className="tb-cell">Вчера, 23:23</div>
							<div className="tb-cell">16 июля, 2012</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
							<div className="tb-cell typeShort"><i className="ic-status itemNonActive" /></div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 typeBlack textSize170">
									<a className="widget-avatar-img-link" href="javascript:void(0);">
										<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
									</a>
									<div className="widget-avatar-info">
										<a className="widget-avatar-name" href="javascript:void(0);">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</a>
									</div>
								</div>
							</div>
							<div className="tb-cell">11 Августа</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
							<div className="tb-cell typeShort"><i className="ic-status itemNonActive" /></div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 textSize170 typeBlank">
									<span className="widget-avatar-img" />
									<div className="widget-avatar-info">
										<span className="widget-avatar-name">{t('Неавторизованный')}</span>
									</div>
								</div>
							</div>
							<div className="tb-cell">2 Апреля</div>
							<div className="tb-cell">15 Сентября, 2014</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
							<div className="tb-cell typeShort"><i className="ic-status itemNonActive" /></div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 typeBlack textSize170">
									<a className="widget-avatar-img-link" href="javascript:void(0);">
										<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
									</a>
									<div className="widget-avatar-info">
										<a className="widget-avatar-name" href="javascript:void(0);">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</a>
									</div>
								</div>
							</div>
							<div className="tb-cell">8 Мая</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
							<div className="tb-cell typeShort"><i className="ic-status itemNonActive" /></div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 textSize170 typeBlank">
									<span className="widget-avatar-img" />
									<div className="widget-avatar-info">
										<span className="widget-avatar-name">{t('Неавторизованный')}</span>
									</div>
								</div>
							</div>
							<div className="tb-cell">6 Июля</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
							<div className="tb-cell typeShort"><i className="ic-status itemNonActive" /></div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>
					</div>
				</div>

				<div className="photo-manage">
					<Btn value={t('Показать ещё')} klass="btn-base btn-sky ** width-300" />

					<div className="widget-iconWithText-group">
						<a className="widget-iconWithText-wrap link-blue" href="javascript:void(0);" role="button" tabIndex="0" download>
							<i className="widget-iconDownload widget-iconWithText size18 ** icon-download" />
							<span className="widget-iconWithText-value">{t('Экспорт в MS Excel')}</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
);
