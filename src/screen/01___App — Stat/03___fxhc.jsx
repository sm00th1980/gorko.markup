import React     from 'react';
import Btn       from 'component/Btn/';
import Form      from 'component/Form/';
import Check     from 'component/Check/';
import Input     from 'component/Input/';
import Select    from 'component/Select/';
import FormGroup from 'component/FormGroup/';

import SubMenu             from 'component/static/SubMenu';
import SubMenuData         from './data/SubMenuData';
import SelectDataAuto      from './data/SelectDataAuto';
import SelectDataPeriod    from './data/SelectDataPeriod';
import SelectDataDayStart  from './data/SelectDataDayStart';
import SelectDataDayFinish from './data/SelectDataDayFinish';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<SubMenu data={SubMenuData} />

		<div className="flex-wrap-bg typeActionPanel">
			<div className="flex-wrap">
				<div className="actionPanel-l"></div>
				<div className="actionPanel-title">{t('Статистика')}</div>
				<div className="actionPanel-r"></div>
			</div>
		</div>

		<div className="flex-wrap-bg typeSeparate">
			<div className="flex-wrap">
				<Form klass="app-manage">
					<div className="form-line typeSmall">
						<FormGroup link="filter">
							<Select klass="** widthFlex-180" items={SelectDataAuto} />
							<Select klass="** widthFlex-120" items={SelectDataPeriod} />
							<Select klass="typeCal ** widthFlex-130" items={SelectDataDayStart} disabled />
							<Select klass="typeCal ** widthFlex-130" items={SelectDataDayFinish} disabled />
							<Check value={t('Посмотрели контакты')} checked />
						</FormGroup>

						<Btn value={t('Показать')} klass="btn-small btn-green-f btn-gray-f" mode="waiting" />
					</div>
				</Form>
			</div>
		</div>

		<div className="flex-wrap-bg typeOverflow">
			<div className="flex-wrap">
				<div className="tb ** -sectStatCar">
					<div className="tb-row tb-title">
						<div className="tb-cell typeLong">{t('Автомобили')}</div>
						<div className="tb-cell typeLong">{t('Посетители')}</div>
						<div className="tb-cell">{t('Время')}</div>
						<div className="tb-cell">{t('Дата свадьбы')}</div>
						<div className="tb-cell typeShort">{t('Посмотревшие контакты')}</div>
					</div>

					<div className="app-pane">
						<div className="tb-row">
							<div className="tb-cell typeLong">Porsche Cayenne</div>
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 textSize170 typeBlank">
									<span className="widget-avatar-img" />
									<div className="widget-avatar-info">
										<span className="widget-avatar-name">{t('Неавторизованный')}</span>
									</div>
								</div>
							</div>
							<div className="tb-cell">Сегодня, 2:23</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">Мерседес 220 Пульман лимузин седан серебристый</div>
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 typeBlack textSize170">
									<a className="widget-avatar-img-link" href="javascript:void(0);">
										<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
									</a>
									<div className="widget-avatar-info">
										<a className="widget-avatar-name" href="javascript:void(0);">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</a>
									</div>
								</div>
							</div>
							<div className="tb-cell">Вчера, 23:23</div>
							<div className="tb-cell">16 июля, 2012</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">Cadillac Escalade гос № 122</div>
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 typeBlack textSize170">
									<a className="widget-avatar-img-link" href="javascript:void(0);">
										<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
									</a>
									<div className="widget-avatar-info">
										<a className="widget-avatar-name" href="javascript:void(0);">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</a>
									</div>
								</div>
							</div>
							<div className="tb-cell">11 Августа</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">Hummer H2 Mega</div>
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 textSize170 typeBlank">
									<span className="widget-avatar-img" />
									<div className="widget-avatar-info">
										<span className="widget-avatar-name">{t('Неавторизованный')}</span>
									</div>
								</div>
							</div>
							<div className="tb-cell">2 Апреля</div>
							<div className="tb-cell">15 Сентября, 2014</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">Ford Excursion</div>
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 typeBlack textSize170">
									<a className="widget-avatar-img-link" href="javascript:void(0);">
										<img className="widget-avatar-img" id="" src="/img/cap/avatar/avatar—etc.svg" alt="" />
									</a>
									<div className="widget-avatar-info">
										<a className="widget-avatar-name" href="javascript:void(0);">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</a>
									</div>
								</div>
							</div>
							<div className="tb-cell">8 Мая</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>

						<div className="tb-row">
							<div className="tb-cell typeLong">Крайслер седан Роллс Ройс</div>
							<div className="tb-cell typeLong">
								<div className="widget-avatar-wrap size30 textSize170 typeBlank">
									<span className="widget-avatar-img" />
									<div className="widget-avatar-info">
										<span className="widget-avatar-name">{t('Неавторизованный')}</span>
									</div>
								</div>
							</div>
							<div className="tb-cell">6 Июля</div>
							<div className="tb-cell itemNonActive">{t('Не указана')}</div>
							<div className="tb-cell typeShort"><i className="ic-status" /></div>
						</div>
					</div>
				</div>

				<div className="photo-manage">
					<Btn value={t('Показать ещё')} klass="btn-base btn-sky btn-gray-f ** width-300" mode="waiting" />

					<div className="widget-iconWithText-group">
						<a className="widget-iconWithText-wrap link-blue" href="javascript:void(0);" role="button" tabIndex="0" download>
							<i className="widget-iconDownload widget-iconWithText size18 ** icon-download" />
							<span className="widget-iconWithText-value">{t('Экспорт в MS Excel')}</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
);