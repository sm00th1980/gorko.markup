import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<span className="reg-title" dangerouslySetInnerHTML={{__html: t('Создать аккаунт на <i>Горько!</i>')}} />

				<div className="reg-status -step-3 margin-b-55">
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">1</i>
							<i className="ic-status size24"></i>
						</span>

						<div className="reg-status-wayL">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">2</i>
							<i className="ic-status size24"></i>
						</span>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">3</i>
						</span>

						<div className="reg-status-wayR">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
				</div>

				<div className="reg-pane-title">
					<span className="reg-pane-title-value">{t('Вы создали аккаунт')}</span>
				</div>

				<div className="reg-pane">
					<div className="reg-pane-account-l">
						<div className="reg-pane-account">
							<span className="reg-pane-account-name">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</span>
							<span className="reg-pane-account-role">Обручальные кольцааааааааааааааааааа</span>
						</div>
						<div className="reg-pane-account">
							<span className="reg-pane-account-name">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</span>
							<span className="reg-pane-account-role">Организатор</span>
						</div>
						<div className="reg-pane-account">
							<span className="reg-pane-account-name">Константина Константинопольскаяяяяяяяяяяяяяяяяяяяяяяяяяяя</span>
							<span className="reg-pane-account-role">Ресторан</span>
						</div>

						<a className="widget-iconWithText-wrap link-gray" href="javascript:void(0);" role="button" tabIndex="0">
							<i className="widget-newIconMP widget-iconWithText size20 ** icon-plus"></i>
							<span className="widget-iconWithText-value">{t('Создать ещё один аккаунт')}</span>
						</a>
					</div>
				</div>

				<Btn value={t('Перейти в профиль')} klass="btn-big btn-green-f ** margin-b-25" />
			</div>
		</div>
	</div>
);