import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<span className="reg-title" dangerouslySetInnerHTML={{__html: t('Создать аккаунт на <i>Горько!</i>')}} />

				<div className="reg-status -step-2 margin-b-55">
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">1</i>
							<i className="ic-status size24"></i>
						</span>

						<div className="reg-status-wayL">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">2</i>
							<i className="ic-status size24"></i>
						</span>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">3</i>
						</span>

						<div className="reg-status-wayR">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
				</div>

				<div className="reg-pane-title">
					<span className="reg-pane-title-value">{t('Добавьте информацию о себе')}</span>
					<a className="widget-iconWithText-wrap link-gray itemNonTransparent" href="javascript:void(0);" role="button" tabIndex="0">
						<i className="ic-arrow size12 widget-iconWithText ** icon-arrow"></i>
						<span className="widget-iconWithText-value">{t('Назад')}</span>
					</a>
				</div>

				<Form klass="reg-pane">
					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regName-01COMMON" htmlFor="baseId-regName-01">{t('Имя')}</label>
						<FormGroup link="regName">
							<Input value="Марина" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regSurname-02COMMON" htmlFor="baseId-regSurname-02">{t('Фамилия')}</label>
						<FormGroup link="regSurname">
							<Input value="Козаченко" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regCity-03COMMON" htmlFor="baseId-regCity-03">{t('Город')}</label>
						<FormGroup link="regCity">
							<Input value="Самара" subLabel="Cамарская область, Россия" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regMail-04COMMON" htmlFor="baseId-regMail-04">{t('Эл. почта')}</label>
						<FormGroup link="regMail">
							<Input value="marina-kamushkina@gmail.com" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regGender-05COMMON" htmlFor="">{t('Пол')}</label>
						<FormGroup link="regGender">
							<Radio value={t('Женский')} checked />
							<Radio value={t('Мужской')} />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regRole-06COMMON" htmlFor="">{t('Роль')}</label>
						<FormGroup link="regRole">
							<Radio value={t('Невеста')} checked />
							<Radio value={t('Жена')} />
							<Radio value={t('Гость на свадьбе')} />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regDate-07COMMON" htmlFor="baseId-regDate-07">{t('Дата свадьбы')}</label>
						<FormGroup klass="typeDate" link="regDate">
							<Input klass="** widthFlex-50" />
							<Select klass="** widthFlex-160" items={SelectDataMonth} />
							<Input klass="** widthFlex-70" />
						</FormGroup>
					</div>
				</Form>

				<Btn value={t('Далее')} klass="btn-big btn-green-f ** margin-b-25" />
			</div>
		</div>
	</div>
);