import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal ** width-540">
				<ModalTitle value={t('Удаление заведения')} />

				<div className="modal-notice">
					{t('Вы действительно хотите удалить заведение')}<br />
					{t('«Лунный свет»?')}
				</div>

				<BtnGroup>
					<Btn value={t('Отменить')} klass="btn-base btn-sky ** width-190" />
					<Btn value={t('Удалить')} klass="btn-base btn-red-f ** width-190" />
				</BtnGroup>
			</div>
		</div>
	</div>
);