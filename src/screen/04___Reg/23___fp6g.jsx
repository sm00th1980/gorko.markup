import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}></div>
);

export const Modal = ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="modal-wrap">
			<div className="modal ** width-740">
				<ModalTitle value={t('Редактирование местоположения')} />

				<div className="reg-map-modal"></div>

				<BtnGroup>
					<Btn value={t('Отменить')} klass="btn-base btn-sky ** width-190" />
					<Btn value={t('Сохранить')} klass="btn-base btn-green-f ** width-190" />
				</BtnGroup>
			</div>
		</div>
	</div>
);