import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<span className="reg-title" dangerouslySetInnerHTML={{__html: t('Создать аккаунт на <i>Горько!</i>')}} />

				<div className="reg-status -step-2 margin-b-55">
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">1</i>
							<i className="ic-status size24"></i>
						</span>

						<div className="reg-status-wayL">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">2</i>
							<i className="ic-status size24"></i>
						</span>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">3</i>
						</span>

						<div className="reg-status-wayR">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
				</div>

				<div className="reg-pane-title">
					<span className="reg-pane-title-value">{t('Добавьте информацию о себе')}</span>
					<a className="widget-iconWithText-wrap link-gray itemNonTransparent" href="javascript:void(0);" role="button" tabIndex="0">
						<i className="ic-arrow size12 widget-iconWithText ** icon-arrow"></i>
						<span className="widget-iconWithText-value">{t('Назад')}</span>
					</a>
				</div>

				<Form klass="reg-pane">
					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regPersonType-01COMMON" htmlFor="">{t('Отображать как')}</label>
						<FormGroup klass="typeColumn" link="regPersonType">
							<Radio value={t('Частное лицо')} checked />
							<Radio value={t('Организацию (команду)')} />
							<span className="form-hint ** -sectReg">{t('В вашем профиле и каталогах будет отображаться название организации вместо вашего имени.')}</span>
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regName-02COMMON" htmlFor="baseId-regName-01">{t('Имя')}</label>
						<FormGroup link="regName">
							<Input value="Марина" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regSurname-03COMMON" htmlFor="baseId-regSurname-02">{t('Фамилия')}</label>
						<FormGroup link="regSurname">
							<Input value="Козаченко" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regCity-04COMMON" htmlFor="baseId-regCity-03">{t('Город')}</label>
						<FormGroup link="regCity">
							<Input value="Самара" subLabel="Cамарская область, Россия" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regMail-05COMMON" htmlFor="baseId-regMail-04">{t('Эл. почта')}</label>
						<FormGroup link="regMail">
							<Input value="marina-kamushkina@gmail.com" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regTel-06COMMON" htmlFor="baseId-regTel-05">{t('Телефон')}</label>
						<div className="flexForm-group">
							<div className="flexForm-input typeTel">
								<input className="flexForm-input-area" id="baseId-regTel-05" type="text" name="nameId-regTel-06COMMON" aria-labelledby="ariaId-regTel-06COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '100px'}} />
								{/*
								<input className="flexForm-input-area" id="baseId-regTel-05" type="text" name="nameId-regTel-05COMMON" aria-labelledby="ariaId-regTel-05COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '110px'}} />
								<input className="flexForm-input-area" id="baseId-regTel-05" type="text" name="nameId-regTel-05COMMON" aria-labelledby="ariaId-regTel-05COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '120px'}} />
								<input className="flexForm-input-area" id="baseId-regTel-05" type="text" name="nameId-regTel-05COMMON" aria-labelledby="ariaId-regTel-05COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '130px'}} />
								*/}
								<div className="widget-flag-wrap">
									<i className="widget-flag" data-iso="RU"></i>
									<span className="widget-flag-code itemActive">+7</span>
									{/*
									<span className="widget-flag-code itemActive">+77</span>
									<span className="widget-flag-code itemActive">+777</span>
									<span className="widget-flag-code itemActive">+7777</span>
									*/}
									<i className="widget-formSelect ** -formSelectTelephone ** icon-formSelect"></i>
								</div>
							</div>
						</div>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regGender-07COMMON" htmlFor="">{t('Пол')}</label>
						<FormGroup link="regGender">
							<Radio value={t('Женский')} checked />
							<Radio value={t('Мужской')} />
						</FormGroup>
					</div>
				</Form>

				<Btn value={t('Далее')} klass="btn-big btn-green-f btn-gray-f ** margin-b-25" mode="waiting" />
			</div>
		</div>
	</div>
);