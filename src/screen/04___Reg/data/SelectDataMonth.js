export default [
	{value: '', title: 'Месяц'},
	{value: 'Январь'},
	{value: 'Февраль'},
	{value: 'Март'},
	{value: 'Апрель'},
	{value: 'Май'},
	{value: 'Июнь'},
	{value: 'Июль'},
	{value: 'Август'},
	{value: 'Сентябрь'},
	{value: 'Октябрь'},
	{value: 'Ноябрь'},
	{value: 'Декабрь'}
];