import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<span className="reg-title" dangerouslySetInnerHTML={{__html: t('Создать аккаунт на <i>Горько!</i>')}} />

				<div className="reg-status -step-2 margin-b-55">
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">1</i>
							<i className="ic-status size24"></i>
						</span>

						<div className="reg-status-wayL">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">2</i>
							<i className="ic-status size24"></i>
						</span>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">3</i>
						</span>

						<div className="reg-status-wayR">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
				</div>

				<div className="reg-pane-title">
					<span className="reg-pane-title-value">{t('Добавьте информацию о вашем бизнесе')}</span>
					<a className="widget-iconWithText-wrap link-gray itemNonTransparent" href="javascript:void(0);" role="button" tabIndex="0">
						<i className="ic-arrow size12 widget-iconWithText ** icon-arrow"></i>
						<span className="widget-iconWithText-value">{t('Назад')}</span>
					</a>
				</div>

				<Form klass="reg-pane">
					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regNameCompany-01COMMON" htmlFor="baseId-regNameCompany-01">{t('Название')}</label>
						<FormGroup link="regNameCompany">
							<Input />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regHall-02COMMON" htmlFor="">{t('Банкетные залы')}</label>
						<FormGroup link="regHall">
							<Radio value={t('Один')} />
							<Radio value={t('Несколько')} checked />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regCity-03COMMON" htmlFor="baseId-regCity-03">{t('Город')}</label>
						<FormGroup link="regCity">
							<Input value="Самара" subLabel="Cамарская область, Россия" icon="loader">
								<div className="popover itemPosB typeSlim typeWide">
									<div className="popover-scroll-wrap size136">
										<div className="popover-scroll">
											<div className="popover-content">
												<ul className="link-blue-f-wrap typeTextCrop">
													<li className="link-blue-f"><b>Сам</b>ара (Самарская область, Россия)</li>
													<li className="link-blue-f">Москва (Московская область, Россия)</li>
													<li className="link-blue-f">Екатеринбург (Свердловская область, Россия)</li>
													<li className="link-blue-f">Комсомольск-на-Амуре (Хабаровский край, Россия)</li>
													<li className="link-blue-f">Самара (Самарская область, Россия)</li>
													<li className="link-blue-f">Москва (Московская область, Россия)</li>
													<li className="link-blue-f">Екатеринбург (Свердловская область, Россия)</li>
													<li className="link-blue-f">Комсомольск-на-Амуре (Хабаровский край, Россия)</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</Input>
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regAddress-04COMMON" htmlFor="baseId-regAddress-04">{t('Адрес')}</label>
						<FormGroup klass="typeColumn" link="regAddress">
							<Input />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regMail-05COMMON" htmlFor="baseId-regMail-05">{t('Эл. почта')}</label>
						<FormGroup link="regMail">
							<Input value="marina-kamushkina@gmail.com" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regTel-06COMMON" htmlFor="baseId-regTel-06">{t('Телефон')}</label>
						<div className="flexForm-group">
							<div className="flexForm-input typeTel">
								<input className="flexForm-input-area" id="baseId-regTel-06" type="text" name="nameId-regTel-06COMMON" aria-labelledby="ariaId-regTel-06COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '100px'}} />
								{/*
								<input className="flexForm-input-area" id="baseId-regTel-05" type="text" name="nameId-regTel-05COMMON" aria-labelledby="ariaId-regTel-05COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '110px'}} />
								<input className="flexForm-input-area" id="baseId-regTel-05" type="text" name="nameId-regTel-05COMMON" aria-labelledby="ariaId-regTel-05COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '120px'}} />
								<input className="flexForm-input-area" id="baseId-regTel-05" type="text" name="nameId-regTel-05COMMON" aria-labelledby="ariaId-regTel-05COMMON" defaultValue="9061272729" placeholder=" " style={{paddingLeft: '130px'}} />
								*/}
								<div className="widget-flag-wrap">
									<i className="widget-flag" data-iso="RU"></i>
									<span className="widget-flag-code itemActive">+7</span>
									{/*
									<span className="widget-flag-code itemActive">+77</span>
									<span className="widget-flag-code itemActive">+777</span>
									<span className="widget-flag-code itemActive">+7777</span>
									*/}
									<i className="widget-formSelect ** -formSelectTelephone ** icon-formSelect"></i>
								</div>

								<div className="popover itemPosB typeSlim typeWide">
									<div className="popover-scroll-wrap size136">
										<div className="popover-scroll">
											<div className="popover-content">
												<ul className="link-blue-f-wrap typeTextCrop">
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="RU" data-code="+7"></i>
														<span className="widget-flag-val">{t('Россия')}</span>
														<span className="widget-flag-code">+7</span>
													</li>
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="AU" data-code="+43"></i>
														<span className="widget-flag-val">{t('Австралия')}</span>
														<span className="widget-flag-code">+43</span>
													</li>
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="AD" data-code="+213"></i>
														<span className="widget-flag-val">{t('Алжир')}</span>
														<span className="widget-flag-code">+213</span>
													</li>
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="AF" data-code="+1684"></i>
														<span className="widget-flag-val">{t('Длинное название')}</span>
														<span className="widget-flag-code">+1684</span>
													</li>
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="RU" data-code="+7"></i>
														<span className="widget-flag-val">{t('Россия')}</span>
														<span className="widget-flag-code">+7</span>
													</li>
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="AU" data-code="+43"></i>
														<span className="widget-flag-val">{t('Австралия')}</span>
														<span className="widget-flag-code">+43</span>
													</li>
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="AD" data-code="+213"></i>
														<span className="widget-flag-val">{t('Алжир')}</span>
														<span className="widget-flag-code">+213</span>
													</li>
													<li className="link-blue-f">
														<i className="widget-flag" data-iso="AF" data-code="+1684"></i>
														<span className="widget-flag-val">{t('Длинное название')}</span>
														<span className="widget-flag-code">+1684</span>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</Form>

				<div className="widget-iconWithText-wrap link-blue ** margin-b-25" role="button" tabIndex="0">
					<i className="widget-newIconMP widget-iconWithText size20 ** icon-plus"></i>
					<span className="widget-iconWithText-value">{t('Добавить ещё заведение')}</span>
				</div>

				<Btn value={t('Далее')} klass="btn-big btn-green-f ** margin-b-25" />
			</div>
		</div>
	</div>
);