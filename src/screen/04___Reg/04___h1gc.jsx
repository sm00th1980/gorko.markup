import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<span className="reg-title" dangerouslySetInnerHTML={{__html: t('Создать аккаунт на <i>Горько!</i>')}} />

				<div className="reg-status -step-2 margin-b-55">
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">1</i>
							<i className="ic-status size24"></i>
						</span>

						<div className="reg-status-wayL">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">2</i>
							<i className="ic-status size24"></i>
						</span>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">3</i>
						</span>

						<div className="reg-status-wayR">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
				</div>

				<div className="reg-pane-title">
					<span className="reg-pane-title-value">{t('Выберите категорию')}</span>
					<a className="widget-iconWithText-wrap link-gray itemNonTransparent" href="javascript:void(0);" role="button" tabIndex="0">
						<i className="ic-arrow size12 widget-iconWithText ** icon-arrow"></i>
						<span className="widget-iconWithText-value">{t('Назад')}</span>
					</a>
				</div>

				<Form klass="reg-pane" link="regCategory">
					<div className="form-line">
						<FormGroup klass="typeColumn">
							<Radio value={t('Фотографы')} />
							<Radio value={t('Видеографы')} />
							<Radio value={t('Ведущие')} />
							<Radio value={t('Диджеи')} />
							<Radio value={t('Рестораны')} />
						</FormGroup>

						<FormGroup klass="typeColumn">
							<Radio value={t('Музыканты')} />
							<Radio value={t('Стилисты')} />
							<Radio value={t('Оформители')} />
							<Radio value={t('Артисты')} />
							<Radio value={t('Теплоходы')} />
						</FormGroup>

						<FormGroup klass="typeColumn">
							<Radio value={t('Хореографы')} />
							<Radio value={t('Организаторы')} />
							<Radio value={t('Авто / Лимузины')} />
							<Radio value={t('Свадебные платья')} />
							<Radio value={t('Обручальные кольца')} />
						</FormGroup>
					</div>
				</Form>

				<Btn value={t('Далее')} klass="btn-big btn-green-f ** margin-b-25" />
			</div>
		</div>
	</div>
);