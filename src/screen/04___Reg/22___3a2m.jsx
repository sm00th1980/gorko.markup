import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<Form klass="reg-pane" link="regAddress">
					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regAddress-01COMMON" htmlFor="baseId-regAddress-01">{t('Адрес')}</label>
						<FormGroup klass="typeColumn">
							<Input />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regAddress-02COMMON" htmlFor="baseId-regAddress-02">{t('Адрес')}</label>
						<FormGroup klass="typeColumn">
							<Input value="Кие" icon="loader">
								<div className="popover itemPosB typeSlim typeWide">
									<div className="popover-scroll-wrap size136">
										<div className="popover-scroll">
											<div className="popover-content">
												<ul className="link-blue-f-wrap typeTextCrop">
													<li className="link-blue-f"><b>Кие</b>вская</li>
													<li className="link-blue-f"><b>Кие</b>тическая</li>
													<li className="link-blue-f"><b>Кие</b>фигурно</li>
													<li className="link-blue-f">Клини<b>кие</b>вская</li>
													<li className="link-blue-f">Миро<b>кие</b>вская</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</Input>
						</FormGroup>
					</div>

					<div style={{height: '173px'}}></div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regAddress-03COMMON" htmlFor="baseId-regAddress-03">{t('Адрес')}</label>
						<FormGroup klass="typeColumn">
							<Input value="Хуй" icon="loader">
								<div className="popover itemPosB typeSlim typeWide">
									<div className="popover-scroll-wrap size136">
										<div className="popover-scroll">
											<div className="popover-content">
												<ul className="link-blue-f-wrap typeTextCrop">
													<li className="popover-content-hint typeError">
														{t('Ничего не найдено ;(')}
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</Input>
						</FormGroup>
					</div>

					<div style={{height: '58px'}}></div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regAddress-04COMMON" htmlFor="baseId-regAddress-04">{t('Адрес')}</label>
						<FormGroup klass="typeColumn">
							<Input value="Киевская, 10, 210" />
							<span className="link-blue ** pull-10 margin-b-10">{t('Показать на карте')}</span>
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regAddress-05COMMON" htmlFor="baseId-regAddress-05">{t('Адрес')}</label>
						<FormGroup klass="typeColumn">
							<Input errorR="Адрес не найден" />
							<span className="link-blue ** pull-10 margin-b-10">{t('Найти вручную')}</span>
						</FormGroup>
					</div>
				</Form>

				<Form klass="reg-pane" link="regMap">
					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regMap-01COMMON" htmlFor="baseId-regMap-01">{t('Отметка на карте')}</label>
						<FormGroup klass="typeColumn">
							<div className="reg-map">
								<span className="ui-errorTag itemPosRArr">{t('Местоположение не найдено')}</span>
							</div>
							<Btn value={t('Найти на карте')} klass="btn-small btn-sky ** -sectReg" />
						</FormGroup>
					</div>

					<div className="form-line">
						<label className="flexForm-label ** widthFlex-120" id="ariaId-regMap-02COMMON" htmlFor="baseId-regMap-02">{t('Отметка на карте')}</label>
						<FormGroup klass="typeColumn">
							<div className="reg-map">
								<span className="ui-errorTag itemPosLArr">{t('Местоположение не найдено')}</span>
							</div>
							<Btn value={t('Изменить положение маркера')} klass="btn-small btn-sky ** -sectReg" />
						</FormGroup>
					</div>
				</Form>
			</div>
		</div>
	</div>
);