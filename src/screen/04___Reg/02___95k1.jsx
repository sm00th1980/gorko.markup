import React      from 'react';
import Btn        from 'component/Btn/';
import Form       from 'component/Form/';
import Input      from 'component/Input/';
import Radio      from 'component/Radio/';
import Select     from 'component/Select/';
import BtnGroup   from 'component/BtnGroup/';
import FormGroup  from 'component/FormGroup/';
import ModalTitle from 'component/ModalTitle/';

import SelectDataMonth from './data/SelectDataMonth';
import SelectDataRange from './data/SelectDataRange';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap typeColumnCenter">
				<span className="reg-title" dangerouslySetInnerHTML={{__html: t('Создать аккаунт на <i>Горько!</i>')}} />

				<div className="reg-status -step-1 margin-b-55">
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">1</i>
							<i className="ic-status size24"></i>
						</span>

						<div className="reg-status-wayL">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">2</i>
							<i className="ic-status size24"></i>
						</span>
					</div>
					<div className="reg-status-item">
						<span className="reg-status-value ** icon-registration-status-oval">
							<i className="reg-status-value-bg">3</i>
						</span>

						<div className="reg-status-wayR">
							<i className="reg-status-way-dots ** icon-registration-status-way-dots"></i>
						</div>
					</div>
				</div>

				<div className="reg-pane-title">
					<span className="reg-pane-title-value">{t('Выберите тип аккаунта')}</span>
				</div>

				<Form klass="reg-pane" link="regAccountType">
					<div className="reg-pane-type">
						<div className="reg-pane-type-card itemActive">
							<span className="reg-pane-type-title">{t('Готовитесь к свадьбе?')}</span>
							<Radio checked />
							<span className="reg-pane-type-name">{t('Личный')}</span>
							<span className="reg-pane-type-desc">{t('Невеста, жених, молодожены, гости на свадьбе...')}</span>
						</div>

						<div className="reg-pane-type-card">
							<span className="reg-pane-type-title">{t('Хотите продавать на Горько?')}</span>
							<Radio />
							<span className="reg-pane-type-name">{t('Бизнес')}</span>
							<span className="reg-pane-type-desc">{t('Фотографы, видеографы и другие профессионалы, а также владельцы бизнеса')}</span>
						</div>
					</div>
				</Form>

				<Btn value={t('Далее')} klass="btn-big btn-green-f ** margin-b-25" />
			</div>
		</div>
	</div>
);