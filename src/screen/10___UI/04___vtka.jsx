import React from 'react';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			{/*
			<div className="flex-wrap" style="height: 100px;">
				<div className="widget-loader-wrap size40 typeVinous">
					<i className="widget-loader ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap" style="height: 100px;">
				<div className="widget-loader-wrap size40">
					<i className="widget-loader ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap" style="height: 100px;">
				<div className="widget-loader-wrap size40 typeGray">
					<i className="widget-loader ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap" style="height: 100px;">
				<div className="widget-loader-wrap size17">
					<i className="widget-loader ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap" style="height: 100px;">
				<div className="widget-loader-wrap size17 typeVinous">
					<i className="widget-loader ** icon-loader"></i>
				</div>
			</div>
			*/}
			<div className="flex-wrap">
				<div className="widget-loaderNEW-wrap size40">
					<i className="widget-loaderNEW ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap">
				<div className="widget-loaderNEW-wrap size40 typeGray">
					<i className="widget-loaderNEW ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap">
				<div className="widget-loaderNEW-wrap size40 typeVinous">
					<i className="widget-loaderNEW ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap">
				<div className="widget-loaderNEW-wrap size17">
					<i className="widget-loaderNEW ** icon-loader"></i>
				</div>
			</div>
			<div className="flex-wrap">
				<div className="widget-loaderNEW-wrap size17 typeVinous">
					<i className="widget-loaderNEW ** icon-loader"></i>
				</div>
			</div>
			<br />
			<br />
			<br />
			<div className="flex-wrap">
				<i className="ic-loader size48 ** icon-loader"></i>
				<i className="ic-loader size48 typeGray ** icon-loader"></i>
				<i className="ic-loader size48 typeRed ** icon-loader"></i>
			</div>
			<div className="flex-wrap">
				<i className="ic-loader size22 ** icon-loader"></i>
				<i className="ic-loader size22 typeRed ** icon-loader"></i>
			</div>
		</div>
	</div>
);