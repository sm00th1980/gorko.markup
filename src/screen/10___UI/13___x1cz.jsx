import React     from 'react';
import Form      from 'component/Form/';
import Input     from 'component/Input/';
import FormGroup from 'component/FormGroup/';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<div className="flex-wrap">
				<div className="popover size200 itemPosB" style={{position: 'relative', left: 'auto', transform: 'translateX(0)'}}>
					<div className="popover-content">
						<Form>
							<div className="form-line typeSmall">
								<FormGroup link="popoverCityName">
									<Input klass="typeIcon" icon="loop" placeholder="Название города" />
								</FormGroup>
							</div>
						</Form>

						<div className="popover-content-hint itemDirL">
							{t('Соседние города')}
						</div>

						<ul className="link-blue-f-wrap">
							<li className="link-blue-f link-blue">{t('Все города')}</li>
							<li className="link-blue-f link-blue">{t('Москва')}</li>
							<li className="link-blue-f link-blue">{t('Санкт-Петербург')}</li>
							<li className="link-blue-f link-blue">{t('Краснодар')}</li>
							<li className="link-blue-f link-blue">{t('Ростов-на-Дону')}</li>
							<li className="link-blue-f link-blue">{t('Новосибирск')}</li>
							<li className="link-blue-f link-blue">{t('Саратов')}</li>
						</ul>
					</div>
				</div>

				<div className="popover size200 itemPosB" style={{position: 'relative', left: 'auto', transform: 'translateX(0)'}}>
					<div className="popover-content">
						<Form>
							<div className="form-line typeSmall">
								<FormGroup link="popoverCityName">
									<Input klass="typeIcon" icon="close" placeholder="Название города" />
								</FormGroup>
							</div>
						</Form>

						<span className="popover-content-hint">
							{t('Начните вводить название города')}
						</span>
					</div>
				</div>

				<div className="popover size200 itemPosB" style={{position: 'relative', left: 'auto', transform: 'translateX(0)'}}>
					<div className="popover-content">
						<Form>
							<div className="form-line typeSmall">
								<FormGroup link="popoverCityName">
									<Input icon="loader" value="Хуй" placeholder="Название города" />
								</FormGroup>
							</div>
						</Form>

						<span className="popover-content-hint">
							{t('Начните вводить название города')}
						</span>
					</div>
				</div>

				<div className="popover size200 itemPosB" style={{position: 'relative', left: 'auto', transform: 'translateX(0)'}}>
					<div className="popover-content">
						<Form>
							<div className="form-line typeSmall">
								<FormGroup link="popoverCityName">
									<Input klass="typeIcon" icon="close" value="Хуй" placeholder="Название города" />
								</FormGroup>
							</div>
						</Form>

						<span className="popover-content-hint">
							{t('Городов с таким названием не нашлось')}
						</span>
					</div>
				</div>

				<div className="popover size200 itemPosB" style={{position: 'relative', left: 'auto', transform: 'translateX(0)'}}>
					<div className="popover-content">
						<Form>
							<div className="form-line typeSmall">
								<FormGroup link="popoverCityName">
									<Input klass="typeIcon" icon="close" value="Мо" placeholder="Название города" />
								</FormGroup>
							</div>
						</Form>

						<ul className="link-blue-f-wrap">
							<li className="link-blue-f link-blue" dangerouslySetInnerHTML={{__html: t('<b>Мо</b>сква')}}></li>
							<li className="link-blue-f link-blue" dangerouslySetInnerHTML={{__html: t('С<b>мо</b>ленск')}}></li>
							<li className="link-blue-f link-blue" dangerouslySetInnerHTML={{__html: t('Ново<b>мо</b>сковск')}}></li>
							<li className="link-blue-f link-blue" dangerouslySetInnerHTML={{__html: t('<b>Мо</b>гилев')}}></li>
							<li className="link-blue-f link-blue" dangerouslySetInnerHTML={{__html: t('До<b>мо</b>дедово')}}></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
);