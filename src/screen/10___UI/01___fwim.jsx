import React     from 'react';
import Form      from 'component/Form/';
import Check     from 'component/Check/';
import Input     from 'component/Input/';
import Radio     from 'component/Radio/';
import Select    from 'component/Select/';
import FormGroup from 'component/FormGroup/';

import SelectDataBlank from './data/SelectDataBlank';

export default ({wrapperClass}) => (
	<div className={wrapperClass}>
		<div className="flex-wrap-bg">
			<Form klass="tb screen-demo" link="nameCamel">
				<div className="tb-row tb-title">
					<div className="tb-cell">Input</div>
					<div className="tb-cell">Input (small)</div>
					<div className="tb-cell">Select</div>
					<div className="tb-cell">Select (small)</div>
				</div>

				<div className="tb-row">
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Input placeholder="Placeholder" />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Input placeholder="Placeholder" />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Select items={SelectDataBlank} />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Select items={SelectDataBlank} />
							</FormGroup>
						</div>
					</div>
				</div>

				<div className="tb-row">
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Input value="Text" />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Input value="Text" />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Select value="Text___01" items={SelectDataBlank} />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Select value="Text___01" items={SelectDataBlank} />
							</FormGroup>
						</div>
					</div>
				</div>

				<div className="tb-row">
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Input value="Text" error />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Input value="Text" error />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Select value="Text___01" items={SelectDataBlank} error />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Select value="Text___01" items={SelectDataBlank} error />
							</FormGroup>
						</div>
					</div>
				</div>

				<div className="tb-row">
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Input value="Text" disabled />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Input value="Text" disabled />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup>
								<Select value="Text___01" items={SelectDataBlank} disabled />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line typeSmall">
							<FormGroup>
								<Select value="Text___01" items={SelectDataBlank} disabled />
							</FormGroup>
						</div>
					</div>
				</div>
			</Form>

			<Form klass="tb screen-demo" link="nameCamel">
				<div className="tb-row tb-title">
					<div className="tb-cell">Checkbox</div>
					<div className="tb-cell">Radio</div>
				</div>

				<div className="tb-row">
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup style={{justifyContent: 'center'}}>
								<Check value={t('Common')} />
								<Check value={t('Checked')} checked />
								<Check value={t('Error')} error />
								<Check value={t('Disabled')} disabled />
							</FormGroup>
						</div>
					</div>
					<div className="tb-cell">
						<div className="form-line">
							<FormGroup style={{justifyContent: 'center'}}>
								<Radio value={t('Common')} />
								<Radio value={t('Checked')} checked />
								<Radio value={t('Error')} error />
								<Radio value={t('Disabled')} disabled />
							</FormGroup>
						</div>
					</div>
				</div>
			</Form>
		</div>
	</div>
);