/**
 * We can hack start script now.
 */

// Setup env first. Webpack config will reference it.
process.env.NODE_ENV = 'development'
require('dotenv').config({silent: true})

var path = require('path')
var paths = require('react-scripts/config/paths')

// We require webpack config here.
var config = require('react-scripts/config/webpack.config.dev')

// Begin hack. Modify webpack config in node modules cache. This example show you how to custom eslint config. You can add 'less-loader' by yourself.
config.eslint.configFile = path.join(paths.appSrc, '.eslintrc.js')

config.module.loaders[1].include = [
	config.module.loaders[1].include,
	/gorko.components\/src\/component/
]

config.resolve.alias.component = 'gorko.components/src/component'

// config.eslint.useEslintrc = true

// Run real start script. Because node wont load modules if they stay in cache. So webpack will got modified config.
require('react-scripts/scripts/start')