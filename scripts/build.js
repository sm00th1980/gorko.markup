/**
 * We can hack build script now.
 * This is similar to 'start.js'. Be sure you have compatible config to avoid build time bug.
 */

process.env.NODE_ENV = 'production'
require('dotenv').config({silent: true})

var path = require('path')
var paths = require('react-scripts/config/paths')
var config = require('react-scripts/config/webpack.config.prod')

config.eslint.configFile = path.join(paths.appSrc, '.eslintrc.js')
// config.eslint.useEslintrc = true // I don't know why this line not work!

require('react-scripts/scripts/build')